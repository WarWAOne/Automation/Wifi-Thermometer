#include <SoftwareSerial.h>

SoftwareSerial ESP8266(1, 0);

// Wifi
String wifiName = "TheLightningBis"; 
String pass = "99_PauLo_96"; 
bool done = false;

// Temperature
int tempPin = 2;
float temp;

//int note = 330;

void setup()
{
  Serial.begin(9600);
  ESP8266.begin(115200);  
  delay(500);
  initESP8266();
}

void loop()
{
    
   /*while(ESP8266.available())
   {    
     Serial.println(ESP8266.readString());
   } */
   
  temp = analogRead(tempPin);
  //temp = temp * 0,48828125;
  float mv = (temp*1100)/1024.0;
 // float mv = (temp/1024.0)*5000;
  //float cel = mv/100;
  float cel = (mv-50)/10;

  Serial.print("Il fait ");
  Serial.print(cel);
  Serial.print(" C");
  Serial.println();
  delay(9000);  
}

void initESP8266()
{  
  sendToESP8266("AT+RST");

// Init
  delay(1000);
  sendToESP8266("AT");
  done = ESP8266.find("OK");
   if(!done){
    delay(1000);
    done = ESP8266.find("OK");
  }

// Client Mod
  sendToESP8266("AT+CWMODE=1");
  done = ESP8266.find("OK");
  if(!done){
    delay(1000);
    done = ESP8266.find("OK");
  }

// Connection to network
  sendToESP8266("AT+CWJAP=\"" + wifiName + "\",\"" + pass + "\"");
  done = ESP8266.find("OK");
  while(!done){
    delay(1000);
    done = ESP8266.find("OK");
  }

// Multiple connections mod
  sendToESP8266("AT+CIPMUX=1");
  done = ESP8266.find("OK");
  if(!done){
    delay(1000);
    done = ESP8266.find("OK");
  }

// Show ip adress
  sendToESP8266("AT+CIFSR");
  done = ESP8266.find("STAIP");
  if(!done){
    delay(1000);
    done = ESP8266.find("OK");
  }
  
 /* recoitDuESP8266(2000);
  Serial.println("**********************************************************");
  sendToESP8266("AT+CWMODE=3");
  recoitDuESP8266(5000);
  Serial.println("**********************************************************");
  sendToESP8266("AT+CWJAP=\""+ wifiName + "\",\"" + pass +"\"");
  recoitDuESP8266(10000);
  Serial.println("**********************************************************");
  sendToESP8266("AT+CIFSR");
  recoitDuESP8266(1000);
  Serial.println("**********************************************************");
  sendToESP8266("AT+CIPMUX=1");   
  recoitDuESP8266(1000);
  Serial.println("**********************************************************");
  sendToESP8266("AT+CIPSERVER=1,80");
  recoitDuESP8266(1000);
  Serial.println("**********************************************************");
  Serial.println("***************** INITIALISATION TERMINEE ****************");
  Serial.println("**********************************************************");
  Serial.println("");  */
}


void sendToESP8266(String command)
{  
  ESP8266.println(command);
}

void recoitDuESP8266(const int timeout)
{
  String reponse = "";
  long int time = millis();
  while( (time+timeout) > millis())
  {
    while(ESP8266.available())
    {
      char c = ESP8266.read();
      reponse+=c;
    }
  }
  Serial.print(reponse);   
}

